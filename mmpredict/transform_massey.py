import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import logging
import random
import os

from sklearn.base import BaseEstimator, TransformerMixin

from mmpredict.dates import regular_season_start

logger = logging.getLogger('luigi-interface')


class DayTransformer(TransformerMixin):
    def __init__(self, field_year, field_day, new_field):
        """
        Called when initializing the classifier
        """
        self.field_year = field_year
        self.field_day = field_day
        self.new_field = new_field

    def check_field(self, X, field):
        if (field not in X):
            raise ValueError('No field {} in actual dataframe'.format(field))

    def convert_day(self, year, day):
        if year not in regular_season_start:
            raise ValueError('No start date for year {}'.format(year))
        return regular_season_start[year] + timedelta(days=int(day))

    def transform(self, X, **transform_params):
        self.check_field(X, self.field_year)
        self.check_field(X, self.field_day)

        X[self.new_field] = pd.to_datetime(
            X.apply(lambda row: self.convert_day(row[self.field_year], row[self.field_day]), axis=1)
            )
        return X

    def fit(self, X, y=None, **fit_params):
        return self


class Sys2ColsTransformer(TransformerMixin):
    def __init__(self, max_tournament_day, min_year = 2003, max_year = 2017):
        """
        Called when initializing the classifier
        """
        self.teams = None
        self.systems = None
        self.min_year = min_year
        self.max_year = max_year
        self.max_tournament_day = max_tournament_day
        pass

    def check_field(self, X, field):
        if (field not in X):
            raise ValueError('No field {} in actual dataframe'.format(field))

    def convert_day(self, year, day):
        if year not in regular_season_start:
            raise ValueError('No start date for year {}'.format(year))
        return regular_season_start[year] + timedelta(days=int(day))

    def calc_comp_index(self, X):
        self.comp_index = ["_".join(map(str, item)) for item in X.index.values]


    def get_all_orank(self, X, year, team, day):
        key = "_".join(map(str, (year, team, day, "orank")))
        if key in self.comp_index:
            return X.ix[(year, team, day, "orank")]
        return np.array([])

    def calc_all_days(self, X):
        systems_count = len(self.systems)
        teams_count = len(self.teams)
        shape_0 = (self.max_year - self.min_year) * teams_count * self.max_tournament_day
        i = 0
        res = np.zeros((shape_0, 3 + systems_count))
        start_time = datetime.now()
        for season in range(self.min_year, self.max_year):
            for team in self.teams:
                last_orank = {}
                for sys in self.systems:
                    last_orank[sys] = 0
                for day in range(1, self.max_tournament_day+1):
                    #get orank
                    row = self.get_all_orank(X, season, team, day)
                    if row.shape[0] > 0:
                        for sys in self.systems:
                            #check massey data
                            if row[sys] > 0:
                                last_orank[sys] = row[sys]
                                #assign data
                    #print(last_orank.values())
                    res[i] = [season, team, day] + list(last_orank.values())
                    i += 1
                    if not (i % 5000):
                        logger.info('iteration # {} of {}, time {} '.format(i, shape_0, datetime.now() - start_time))

        logger.info("Done. Time elapsed {}".format(datetime.now() - start_time))
        return pd.DataFrame(res, columns=["season", "team", "day"]+self.systems)

    def transform(self, X, **transform_params):
        self.teams = X["team"].unique()
        self.systems = sorted(X["sys_name"].unique())
        pivoted_X = X.pivot_table(
            index=["season", "team", "rating_day_num"],
            columns=["sys_name"],
            values=["orank"],
            fill_value=0).stack(level=0)

        self.calc_comp_index(pivoted_X)

        return self.calc_all_days(pivoted_X)

    def fit(self, X, y=None, **fit_params):
        return self


class MergeTransformer(TransformerMixin):
    def __init__(self, right_df, left_on, right_on):
        """
        Called when initializing the classifier
        """
        self.right_df = right_df
        self.left_on = left_on
        self.right_on = right_on


    def transform(self, X, **transform_params):
        return pd.merge(X, self.right_df,
             how='left',
             left_on=self.left_on,
             right_on=self.right_on,
             copy=False
                        )

    def fit(self, X, y=None, **fit_params):
        return self


class RandomWinnerTransformer(TransformerMixin):
    def __init__(self, Wteam="Wteam", Lteam="Lteam"):
        """
        Called when initializing the classifier
        """
        self.Wteam = Wteam
        self.Lteam = Lteam

    def transform(self, X, **transform_params):
        X["win"] = 1
        for i, idx in enumerate(X.index):
            win = random.choice([1,0])
            if not win:
                #winner = looser
                X.ix[idx,[self.Wteam, self.Lteam, "win"]] = X.ix[idx, self.Lteam], X.ix[idx, self.Wteam], 0
        return X.rename(columns={self.Wteam: 'team1', self.Lteam: 'team2'})

    def fit(self, X, y=None, **fit_params):
        return self
