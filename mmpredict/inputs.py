import logging
import os

import luigi
import numpy as np
import pandas as pd
from sklearn.pipeline import Pipeline

from mmpredict.base import KaggleData, BaseTask
from mmpredict.transform_massey import Sys2ColsTransformer
from mmpredict.merge_massey import MergeMasseyMixIn
from mmpredict.transform_massey import RandomWinnerTransformer

logger = logging.getLogger('luigi-interface')


class TourneyCompactResults(KaggleData):
    pass


class RegularSeasonCompactResults(KaggleData):
    pass


class SampleSubmission(KaggleData):
    pass


class SampleSubmissionStage2(KaggleData):
    pass


class Massey(KaggleData):
    pass


class MasseyStage2(KaggleData):
    pass


class ECDFResult(KaggleData):
    pass


class MasseyAllDays(BaseTask):
    def requires(self):
        yield Massey()
        yield MasseyStage2()
        yield TourneyCompactResults()

    def run(self):
        df = pd.read_csv(self.input()[0].path)
        df2 = pd.read_csv(self.input()[1].path)

        df = pd.concat([df, df2]).reset_index()

        df = df[df["season"] <= self.target_year.year]

        tornament_results = pd.read_csv(self.input()[2].path)
        tornament_results = tornament_results[tornament_results["Season"] < self.target_year.year]

        max_day = tornament_results["Daynum"].max()

        logger.info('Tournament max day {}.'.format(max_day))

        pipe_munge = Pipeline([
            ('interpolate', Sys2ColsTransformer(max_tournament_day=max_day,
                                                min_year=2003, max_year=self.target_year.year+1))
        ])

        pipe_munge.transform(df).to_csv(self.output().path, index=False)


    def output(self):
        return luigi.LocalTarget(os.path.join(self.output_root, "massey_all_days4.csv"))


class MergeTrainDataMassey(BaseTask, MergeMasseyMixIn):

    def requires(self):
        yield RegularSeasonCompactResults()
        yield TourneyCompactResults()
        yield MasseyAllDays()

    def run(self):
        massey = pd.read_csv(self.input()[2].path)
        min_year = int(massey["season"].min())
        logger.info('Use data starting from {} year.'.format(min_year))

        regular_season = pd.read_csv(self.input()[0].path)
        regular_season = regular_season[regular_season["Season"] >= min_year]
        tournament = pd.read_csv(self.input()[1].path)
        tournament = tournament[tournament["Season"] >= min_year]

        df = pd.concat([regular_season, tournament])#[:200]
        df = df.drop(["Wscore", "Lscore", "Wloc", "Numot"],1)

        pipe = Pipeline([
            ('random_winner', RandomWinnerTransformer())
        ])

        df = pipe.transform(df)
        self.merge(df, massey).to_csv(self.output().path, index=False)

    def output(self):
        return luigi.LocalTarget(os.path.join(self.output_root, "train_massey4.csv"))


class CreateSubmissionDF(BaseTask, MergeMasseyMixIn):

    def requires(self):
        yield SampleSubmission()

    def run(self):
        preds = pd.read_csv(self.input()[0].path)
        _sample = np.zeros((preds.shape[0],3))

        i = 0
        for index, row in preds.iterrows():
            p = list(map(int, str.split(str(row['id']), '_')))
            _sample[i] = p
            i += 1

        df_sample = pd.DataFrame(_sample, columns=["Season", "team1", "team2"])
        df_sample["Daynum"] = 133
        df_sample["win"] = np.nan
        df_sample.to_csv(self.output().path, index=False)

    def output(self):
        return luigi.LocalTarget(os.path.join(self.output_root, "submission.csv"))


class MergeSubmissionDataMassey(BaseTask, MergeMasseyMixIn):
    def requires(self):
        yield CreateSubmissionDF()
        yield MasseyAllDays()

    def run(self):
        df = pd.read_csv(self.input()[0].path)
        massey = pd.read_csv(self.input()[1].path)
        self.merge(df, massey).to_csv(self.output().path, index=False)

    def output(self):
        return luigi.LocalTarget(os.path.join(self.output_root, "submission_massey2.csv"))
