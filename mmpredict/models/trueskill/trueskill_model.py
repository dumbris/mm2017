import numpy as np
from mmpredict.models.trueskill import trueskill


class Player(object):
    pass


def pl_init():
    pl = Player()
    pl.skill = (25.0, 25.0/3.0)
    return pl


def calc(train, preds) -> np.ndarray:
    """Calc predictions for NCAAB games based on TrueSkill.

    Args:
        train: Games dataframe required 2 columns: Wteam, Lteam.
        preds: Kaggle sample submission dataframe.

    Returns:
        dataframe with predictions.

    """

    team = {}

    for index, row in train.iterrows():
        t1 = row['Wteam']
        t2 = row['Lteam']
        if not t1 in team: team[t1] = pl_init()
        if not t2 in team: team[t2] = pl_init()
        team[t1].rank = 1
        #team[t2].rank = 2
        #Use margin of victory
        team[t2].rank = int(row['Wscore']) - int(row['Lscore'])
        trueskill.AdjustPlayers([team[t1], team[t2]])
    res = []
    for i in team:
        res.append(team[i].skill[0])

    all_mu = np.array(res)

    _min = all_mu.min()
    _max = all_mu.max()
    new_max = _max - _min

    for _id in team:
        sk = team[_id].skill[0]
        new_sk = sk - _min
        team[_id].scaled_skill = (new_sk/float(new_max))

    prediction = np.zeros((preds.shape[0], 1))

    i = 0
    for index, row in preds.iterrows():
        p = list(map(int, str.split(str(row['Id']), '_')))
        prediction[i] = 0.5 + (team[p[1]].scaled_skill - team[p[2]].scaled_skill)
        i += 1

    return np.clip(prediction, .01, .99)

