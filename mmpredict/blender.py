import luigi
import pandas as pd
from mmpredict.trueskill import Predict
from mmpredict.inputs import ECDFResult
from mmpredict.base import BaseTask


class Blend(BaseTask):

    def requires(self):
        yield Predict()
        yield ECDFResult()

    def run(self):
        trueskill = pd.read_csv(self.input()[0].path)
        ecdf = pd.read_csv(self.input()[1].path)
        df = pd.concat([trueskill, ecdf], axis=1)

        df.columns = ['Id','Pred1', 'Id2', 'Pred2']
        df["Pred"] = df[["Pred1", "Pred2"]].mean(axis=1)
        print(df.head())
        df[["Id", "Pred"]].to_csv(self.output().path, index=False)

    def output(self):
        return luigi.LocalTarget(self.output_root + "/predictions6.csv")


if __name__ == "__main__":
    luigi.run()
