import luigi
import datetime
import os
from subprocess import Popen, PIPE


class BaseTask(luigi.Task):
    year = datetime.date.today()
    target_year = luigi.DateParameter(default=year)
    #date_interval = luigi.DateIntervalParameter(default=luigi.date_interval.DateInterval(datetime.date.year() - datetime.timedelta(7), datetime.today()))
    output_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "output_data", str(year.strftime('%Y')))
    output_root = luigi.Parameter(default=output_dir)

    def ensure_output_dir(self):
        if not os.path.exists(os.path.dirname(self.output().path)):
            os.makedirs(os.path.dirname(self.output().path))
        return True


class ConsoleTask(BaseTask):
    def console(self, cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        out, err = p.communicate()
        return p.returncode, out, err
    pass


class KaggleData(luigi.ExternalTask):
    file = luigi.Parameter()

    def output(self):
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.file)
        return luigi.LocalTarget(path)

