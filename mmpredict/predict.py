import logging

import luigi
import datetime
import pandas as pd
import numpy as np
import sklearn.metrics as metrics
from sklearn.externals import joblib
from mmpredict.base import BaseTask
from mmpredict.train import GetScaler, RandomForest, LogReg, RandomForest2
from mmpredict.inputs import MergeSubmissionDataMassey, SampleSubmission


logger = logging.getLogger('luigi-interface')

class Predict(BaseTask):

    y = luigi.Parameter(default='win')
    drop = luigi.ListParameter(default=["Season", "Daynum", "team1", "team2", "win"])

    def requires(self):
        yield RandomForest()
        yield GetScaler()
        yield MergeSubmissionDataMassey()
        yield SampleSubmission()

    def run(self):
        model = joblib.load(self.input()[0].path)
        scaler = joblib.load(self.input()[1].path)
        df = pd.read_csv(self.input()[2].path)
        df = df.drop(list(self.drop), axis=1)
        submission = pd.read_csv(self.input()[3].path)
        print(df.head())
        #cv = mm_CV(df)
        logger.info('Got {} games for prediction'.format(df.shape[0]))

        prediction = model.predict_proba(scaler.transform(df))[:, 1]
        submission['pred'] = np.clip(prediction, 0.05, 0.93)
        submission.to_csv(self.output().path, index=False)


class LogRegPred(Predict):

    def requires(self):
        yield LogReg()
        yield GetScaler()
        yield MergeSubmissionDataMassey()
        yield SampleSubmission()

    def output(self):
        return luigi.LocalTarget(self.output_root + "/logreg_predictions.csv")

class RFPred2(Predict):

    def requires(self):
        yield RandomForest2()
        yield GetScaler()
        yield MergeSubmissionDataMassey()
        yield SampleSubmission()

    def output(self):
        return luigi.LocalTarget(self.output_root + "/rf_predictions2.csv")
