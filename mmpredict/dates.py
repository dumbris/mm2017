import sys
from datetime import datetime

regular_season_start = {
    2017: "November 11, 2016",
    2016: "November 13, 2015",
    2015: "November 14, 2014",
    2014: "November 8, 2013",
    2013: "November 9, 2012",
    2012: "November 7, 2011",
    2011: "November 8, 2010",
    2010: "November 9, 2009",
    2009: "November 10, 2008",
    2008: "November 5, 2007",
    2007: "November 7, 2006",
    2006: "November 6, 2005",
    2005: "November 10, 2004",
    2004: "November 10, 2003",
    2003: "November 10, 2002"
}


if sys.version_info < (3, 4):
    regular_season_start = {k: datetime.strptime(v, '%B %d, %Y').date() for k, v in regular_season_start.iteritems()}
else:
    regular_season_start = {k: datetime.strptime(v, '%B %d, %Y').date() for k, v in regular_season_start.items()}
