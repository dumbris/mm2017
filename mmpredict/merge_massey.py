import logging
import pandas as pd
from sklearn.pipeline import Pipeline, FeatureUnion
from mmpredict.transform_massey import MergeTransformer

logger = logging.getLogger('luigi-interface')


class MergeMasseyMixIn:
    index_cols = ["season", "day", 'team']

    def sys_cols(self, systems):
        res = ["W_"+sys for sys in systems]
        res += ["L_"+sys for sys in systems]
        return res

    def rename_massey(self, letter):
        def fun0(name):
            if name in self.index_cols:
                return name
            return letter + name
        return fun0

    def merge(self, df, massey, team1='team1', team2='team2'):

        logger.info('Got {} games in total.'.format(df.shape[0]))
        logger.info('Got {} systems in total.'.format(len(massey.columns.values)))

        print(df.head())

        pipe_munge = Pipeline([
            ('union', FeatureUnion(transformer_list=[
                ('winner', Pipeline([
                    ('merge_winner', MergeTransformer(massey.rename(columns=self.rename_massey("W_")),
                                                      ["Season", "Daynum", team1],
                                                      ["season", "day", 'team']
                                                      )
                     )
                ])
                 ),
                ('looser', Pipeline([
                    ('merge_looser', MergeTransformer(massey.rename(columns=self.rename_massey("L_")),
                                                      ["Season", "Daynum", team2],
                                                      ["season", "day", 'team']
                                                      )
                     )
                ])
                 )

            ]))
        ])

        systems = set(massey.columns.values) - set(self.index_cols)

        to_del = ["Season2", "Daynum2", "Wteam2", "Lteam2", "win2"] + \
                 ["season2", "team22", "day2"]

        cols = ["Season", "Daynum", team1, team2, "win"] + \
               ["season", "team", "day"] + \
               ["W_"+sys for sys in systems] + \
               to_del + \
               ["L_"+sys for sys in systems]

        data = pipe_munge.transform(df)

        df2 = pd.DataFrame(data=data,
                           columns=cols
                           )
        print(df2.head())
        df2 = df2.drop(to_del + ["season", "team", "day"], 1)
        return df2

