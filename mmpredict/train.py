import logging

import luigi
import datetime
import pandas as pd
from sklearn.pipeline import Pipeline, FeatureUnion
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.externals import joblib
from mmpredict.inputs import RegularSeasonCompactResults, SampleSubmission, TourneyCompactResults
from mmpredict.base import BaseTask
from mmpredict.inputs import MergeTrainDataMassey, MergeSubmissionDataMassey
from mmpredict.crossval import mm_CV
import os


logger = logging.getLogger('luigi-interface')


class GetScaler(luigi.ExternalTask):
    file = luigi.Parameter()

    def output(self):
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), self.file)
        return luigi.LocalTarget(path)


class FitScaler(BaseTask):
    y = luigi.Parameter(default='win')
    drop = luigi.Parameter(default=["Season", "Daynum", "team1", "team2", "win"])

    def requires(self):
        yield MergeTrainDataMassey()

    def run(self):
        df = pd.read_csv(self.input()[0].path)
        #TODO limit Seasons here
        y = df[self.y].values
        df = df.drop(list(self.drop), axis=1)
        df = df.dropna()
        logger.info('Got {} games for scaling'.format(df.shape[0]))
        scaler = StandardScaler()
        scaler.fit(df, y)
        joblib.dump(scaler, self.output().path)

    def output(self):
        return luigi.LocalTarget(self.output_root + "/scaler.pkl")


class FitModel(BaseTask):
    y = luigi.Parameter(default='win')
    drop = luigi.ListParameter(default=["Season", "Daynum", "team1", "team2", "win"])

    def requires(self):
        yield MergeTrainDataMassey()
        yield GetScaler()

    def plot_gs(self, grid_search, param_grid, filename="grid.png"):
        fig = plt.figure(figsize=(15,10))
        plt.plot([c.mean_validation_score for c in grid_search.grid_scores_], label="validation error")
        plt.xticks(np.arange(10), param_grid)
        plt.xlabel("grid_param")
        plt.ylabel("Accuracy")
        plt.legend(loc='best')
        plt.savefig(os.path.join(self.output_root, filename), bbox_inches='tight')
        plt.close(fig)

    def grid_search(self, clf, data, y, cv, grid):
        start_time = datetime.datetime.now()
        gs = GridSearchCV(clf, grid, cv=cv, scoring='neg_log_loss')
        gs.fit(data, y)

        logger.info("""Time elapsed: {}\n
                       Best score: {}\n
                       Best estimator: {}\n
                    """.format(datetime.datetime.now() - start_time, gs.best_score_, gs.best_estimator_))
        return gs

    def run(self):
        df = pd.read_csv(self.input()[0].path)
        scaler = joblib.load(self.input()[1].path)
        df = df.dropna()
        #cv = mm_CV(df)
        cv = KFold(n_splits=5, shuffle=True, random_state=241)
        y = df[self.y].values
        df = df.drop(list(self.drop), axis=1)
        logger.info('Got {} games for fiting'.format(df.shape[0]))

        gs = self.fit(scaler.transform(df, y), y, cv)

        joblib.dump(gs.best_estimator_, self.output().path)


class LogReg(FitModel):

    def fit(self, df, y, cv):
        grid = {"logistic__C": 10. ** np.arange(-5, 5)}
        # create feature union
        features = []
        features.append(('pca', PCA(n_components=10)))
        features.append(('select_best', SelectKBest(k=20)))
        feature_union = FeatureUnion(features)
        # create pipeline
        estimators = []
        estimators.append(('feature_union', feature_union))
        estimators.append(('logistic', LogisticRegression(verbose=False)))
        model = Pipeline(estimators)

        gs = self.grid_search(model, df, y, cv, grid)

        self.plot_gs(gs, grid['logistic__C'], 'logistic__C.png')
        return gs

    def output(self):
        return luigi.LocalTarget(self.output_root + "/logistic.pkl")


class RandomForest(FitModel):

    def fit(self, df, y, cv):
        grid = {"randomforest__n_estimators": range(500, 1000, 100)}
        # create feature union
        features = []
        features.append(('pca', PCA(n_components=10)))
        features.append(('select_best', SelectKBest(k=20)))
        feature_union = FeatureUnion(features)
        # create pipeline
        estimators = []
        estimators.append(('feature_union', feature_union))
        estimators.append(('randomforest', RandomForestClassifier(random_state=241)))
        model = Pipeline(estimators)

        gs = self.grid_search(model, df, y, cv, grid)

        self.plot_gs(gs, grid['randomforest__n_estimators'], 'RF_n_estimators.png')
        return gs

    def output(self):
        return luigi.LocalTarget(self.output_root + "/randomforest.pkl")

class RandomForest2(FitModel):

    def fit(self, df, y, cv):
        grid = {"randomforest__n_estimators": range(2000, 3500, 500)}
        # create feature union
        estimators = []
        estimators.append(('randomforest', RandomForestClassifier(random_state=241)))
        model = Pipeline(estimators)

        gs = self.grid_search(model, df, y, cv, grid)

        self.plot_gs(gs, grid['randomforest__n_estimators'], 'RF_n_estimators2.png')
        return gs

    def output(self):
        return luigi.LocalTarget(self.output_root + "/randomforest2.pkl")

if __name__ == "__main__":
    luigi.run()
