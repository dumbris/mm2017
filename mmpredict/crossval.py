import numpy as np


def mm_CV(df, years_in_fold = 2, min_year = 2003, max_year = 2015, regular_season_last_day = 133):

    folds = []

    for season in range(min_year,max_year):
        train_index = np.array([],dtype=int)
        test_index = np.array([],dtype=int)
        for _year in range(season,season+years_in_fold):
            train_index = np.concatenate((
                    train_index,
                    df[(df["Season"] == _year) & (df["Daynum"] <= regular_season_last_day)].index.values.astype(int)
                ))
            if _year == (season + years_in_fold - 1):
                #use a tournament data for last year a test data
                test_index = df[(df["Season"] == _year) & (df["Daynum"] >= regular_season_last_day)].index.values.astype(int)
            else:
                train_index = np.concatenate((
                        train_index,
                        df[(df["Season"] == _year) & (df["Daynum"] > regular_season_last_day)].index.values.astype(int)
                        ))
        folds.append( (train_index, test_index) )

    return folds