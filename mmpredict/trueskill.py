import luigi
import pandas as pd
from mmpredict.models.trueskill.trueskill_model import calc
from mmpredict.inputs import RegularSeasonCompactResults, SampleSubmission, TourneyCompactResults, SampleSubmissionStage2
from mmpredict.base import BaseTask


class Predict(BaseTask):

  def requires(self):
    yield RegularSeasonCompactResults()
    yield TourneyCompactResults()
    yield SampleSubmissionStage2()

  def run(self):
    train = pd.read_csv(self.input()[0].path)
    tourney = pd.read_csv(self.input()[1].path)
    #tourney = tourney[tourney["Season"] < 2016]
    df = pd.concat([train, tourney]).sort_values(["Season", "Daynum"])

    preds = pd.read_csv(self.input()[2].path)

    print(df.head())

    predictions = calc(train=df, preds=preds)
    preds["Pred"] = predictions
    preds.to_csv(self.output().path, index=False)

  def output(self):
    return luigi.LocalTarget(self.output_root + "/true_skill_predictions5.csv")


if __name__ == "__main__":
  luigi.run()
